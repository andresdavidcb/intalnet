<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>INTALNET TELECOMUNICACIONES</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    <link rel="stylesheet" type="text/css" href="css/main.css">
   
    
</head>
<body id="app-layout">
    
    
    <nav class="navbar navbar-default">
      
      <div class="logo" style="background: #fff">
        <img src='img/style/logo_intalnet.jpg'> 
      </div>
      

      <div class="container-fluid">
        <!--<div class="col-md-10 col-md-offset-1">-->
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
        </div>
        <?php 
        $url=$_SERVER['REQUEST_URI'];
        $a='active';
        ?>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li><a href="{{ url('/') }}" class="{{$url=='/'?$a:''}}">INICIO</a></li>
            
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">NUESTRA EMPRESA<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="{{ url('somos') }}" class="{{$url=='somos'?$a:''}}">¿Quiénes somos</a></li>
                <li><a href="{{ url('mision') }}" class="{{$url=='mision'?$a:''}}">Misión</a></li>
                <li><a href="{{ url('vision') }}" class="{{$url=='vision'?$a:''}}">Visión</a></li>
                <li><a href="{{ url('valores') }}" class="{{$url=='valores'?$a:''}}">Valores</a></li>
              </ul>
            </li>
            
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">SERVICIOS<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <!--
                <li><a href="{{ url('camaras') }}" class="{{$url=='camaras'?$a:''}}">Cámaras de seguridad</a></li>
                <li><a href="{{ url('banda_ancha') }}" class="{{$url=='banda_ancha'?$a:''}}">Internet banda ancha</a></li>
                <li><a href="{{ url('internet_dedicado') }}" class="{{$url=='internet_dedicado'?$a:''}}">Internet dedicado</a></li>
                <li><a href="{{ url('accesorios') }}" class="{{$url=='accesorios'?$a:''}}">Accesorios de tecnología</a></li>
                -->
                <li><a href="#" class="{{$url=='camaras'?$a:''}}">Cámaras de seguridad</a></li>
                <li><a href="#" class="{{$url=='banda_ancha'?$a:''}}">Internet banda ancha</a></li>
                <li><a href="#" class="{{$url=='internet_dedicado'?$a:''}}">Internet dedicado</a></li>
                <li><a href="#" class="{{$url=='accesorios'?$a:''}}">Accesorios de tecnología</a></li>
              </ul>
            </li>



            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">REGULACIÓN<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="{{ url('PQR') }}" class="{{$url=='PQR'?$a:''}}">Peticiones, Quejas y Reclamos (PQR)</a></li>
                <li><a href="{{ url('proteccion_al_usuario') }}" class="{{$url=='proteccion_al_usuario'?$a:''}}">Protección al usuario</a></li>
                <li><a href="{{ url('entes_de_control') }}" class="{{$url=='entes_de_control'?$a:''}}">Entes de control</a></li>
                <li><a href="{{ url('politicas') }}" class="{{$url=='politicas'?$a:''}}">Políticas de seguridad</a></li>
              </ul>
            </li>


            <li><a href="{{ url('FAQ') }}" class="{{$url=='FAQ'?$a:''}}">PREGUNTAS FRECUENTES</a></li>
            <li><a href="{{ url('seguridad_en_la_red') }}" class="{{$url=='seguridad_en_la_red'?$a:''}}">INTERNET SANO</a></li>
            <li><a href="{{ url('contacto') }}" class="{{$url=='contacto'?$a:''}}">CONTACTENOS</a></li>
          </ul>
        </div><!-- /.navbar-collapse -->
      <!-- /.container-fluid -->
      </div>
    </nav>


    @yield('content')

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}

<footer style="text-align:center">
  
<strong>INTALNET TELECOMUNICACIONES Nit: 900.888.246-0</strong><br>
<strong>Registro mercantil:</strong> No. 00143072. || <strong>Registro TIC:</strong> No. 96003059<br>
<strong>Teléfono:</strong> 3152522215. || <strong>E-mail:</strong> servicioalcliente@intalnettelecomunicaciones.com<br>
<strong>Nuestras sedes:</strong><br>Carrera 14 # 12-26 Barrio Centro. Valencia - Córdoba<br>
Calle 2 sector las Balsas  Bario 9 e Agosto. Tierralta - Córdoba<br>

  <a href="https://www.facebook.com/valencia.net.90" target="_blank"><img src="img/style/logo_facebook.png"></a>
  <a href="{{url('contacto')}}" target="_blank"><img src="img/style/logo_email.png"></a>
  <a href="https://www.youtube.com/channel/UCLcCfm5o5PL4AS8UishkzQw" target="_blank"><img src="img/style/logo_youtube.png"></a>
  <a href="https://www.mintic.gov.co" title="Ministerio de Tecnologías de la Información y Comunicación" target="_blank"><img src="img/style/mintic.png"></a>

</footer>    
</body>
</html>
