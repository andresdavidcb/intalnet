@extends('layouts.app')
@section('content')

<div class='container col-md-10 col-md-offset-1'>
<div class='col-md-8'>
	<H1>CONTÁCTENOS</H1>
	<img src="img/gallery/contacto.jpg" width="100%">

	<div class="col-md-6">
     <h3>Oficinas Sede Valencia</h3>
     <p>
      Calle 14 # 12 -26<br>
      Barrio Centro<br>
      <strong>Celular: </strong>315 252 2215
   </div>
   <div class="col-md-6">
     <h3>Oficinas Sede Tierralta</h3>
     <p>
       Calle 2 sector las Balsas<br>
       Bario 9 e Agosto.<br>
       <strong>Celular: </strong>320 401 3011
     </p>
   </div>
</div>

<div class='col-md-4'>
	
{!! Form::open(['route' => 'contacto.store','method' => 'POST']) !!}	
	<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
	    <label for='name' class='col-md-12 control-label'>Nombre</label>
	    <div class='col-md-12'>
	    <input required type='text' id='name' name='name' class='form-control' value="{{old('name')}}">
	     @if ($errors->has('name'))
	        <span class='help-block'><strong>{{ $errors->first('name') }}</strong></span>
	    @endif
	    </div>
	</div>

	<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
	    <label for='email' class='col-md-12 control-label'>Correo electrónico</label>
	    <div class='col-md-12'>
	    <input required type='email' id='email' name='email' class='form-control' value="{{old('email')}}">
	     @if ($errors->has('email'))
	        <span class='help-block'><strong>{{ $errors->first('email') }}</strong></span>
	    @endif
	    </div>
	</div>

	<div class="form-group{{ $errors->has('msg') ? ' has-error' : '' }}">
	    <label for='msg' class='col-md-12 control-label'>Mensaje</label>
	    <div class='col-md-12'>
	    <textarea required  id='msg' name='msg' class='form-control' rows="12">{{old('msg')}}</textarea>
	     @if ($errors->has('msg'))
	        <span class='help-block'><strong>{{ $errors->first('msg') }}</strong></span>
	    @endif
	    </div>
	</div>

	<div class="col-md-12">
		<button class='btn-warning' style="width:100%;margin:10px 0">Comentar</button>
	</div>
	<div id='message' class='successs'>
	</div>

	<p>contacto@intalnettelecomunicaciones.com<br></p>

{!!Form::close()!!}
</div>


</div>


@endsection