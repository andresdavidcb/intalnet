@extends('layouts.app')

@section('content')
<div class="container col-md-10 col-md-offset-1">
<H1>PETICIONES, QUEJAS Y RECLAMOS</H1>


<div class="col-md-6">
  <H2 style="text-align:left">INTALNET TELECOMUNICACIONES</H2>
<P>
  <img src="img/gallery/pqr.png"><br>
  <STRONG>Teléfono:</STRONG> <br>(57) 315 2522215<BR>
  <STRONG>Email:</STRONG> <br>contacto@intalnettelecomunicaciones.com<br>
  servicioalcliente@intalnettelecomunicaciones.com <BR>
  <STRONG>Página web:<br></STRONG> www.intalnettelecomunicaciones.com<br>
  <STRONG>Webmaster:<br> </STRONG>Andrés David Coronado Blanco<br>

</P>

</div>



<div class="col-md-6">
{!! Form::open(['route' => 'PQR.store','method' => 'POST']) !!}  
  <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
      <label for='name' class='col-md-12 control-label'>Nombre</label>
      <div class='col-md-12'>
      <input required type='text' id='name' name='name' class='form-control' value="{{old('name')}}">
       @if ($errors->has('name'))
          <span class='help-block'><strong>{{ $errors->first('name') }}</strong></span>
      @endif
      </div>
  </div>

  <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
      <label for='email' class='col-md-12 control-label'>Correo electrónico</label>
      <div class='col-md-12'>
      <input required type='email' id='email' name='email' class='form-control' value="{{old('email')}}">
       @if ($errors->has('email'))
          <span class='help-block'><strong>{{ $errors->first('email') }}</strong></span>
      @endif
      </div>
  </div>

  <div class="form-group{{ $errors->has('msg') ? ' has-error' : '' }}">
      <label for='msg' class='col-md-12 control-label'>PQR:</label>
      <div class='col-md-12'>
      <textarea required  id='msg' name='msg' class='form-control' rows="9"
      placeholder="Escriba aquí sus peticiones, quejas o reclamos de nuestro servicio">{{old('msg')}}</textarea>
       @if ($errors->has('msg'))
          <span class='help-block'><strong>{{ $errors->first('msg') }}</strong></span>
      @endif
      </div>
  </div>

  <div class="col-md-12">
    <button class='btn-warning' style="width:100%;margin:10px 0">Enviar</button>
  </div>
  <div id='message' class='successs'>
  </div>


{!!Form::close()!!}


</div>


</div>
@endsection