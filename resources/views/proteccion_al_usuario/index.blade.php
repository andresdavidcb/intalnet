@extends('layouts.app')

@section('content')
<div class="container col-md-10 col-md-offset-1">
<H1>PROTECCIÓN AL USUARIO</H1>

<H2 style="text-align:left">Señor(a) Usuario:</H2>
  <p>Usted tiene derecho a recibir atención a través de los siguientes canales.</p>
<div class="container col-md-8">
<img src="img/gallery/proteccion.jpg" width="100%">
</div>

<div class="container col-md-4">
  
  <P>
    <H3 style="text-align:left">Canales:</H3>
    <STRONG>Nuestras oficinas de atención al usuario:</STRONG> <br>
  <p>Calle 2 sector las Balsas, junto al acueducto comunitario.<br>
  Bario 9 e Agosto. Tierralta - Córdoba, Colombia</p>
  <p>
    Carrera 14 # 12-26, Barrio centro.<br>
    Valencia, Córdoba
  </p>
  <BR>
    <STRONG>Teléfonos:</STRONG> <br>(57) (57) 315 2522215<BR>
    <STRONG>Email:</STRONG> <br>contacto@intalnettelecomunicaciones.com<br>
    servicioalcliente@intalnettelecomunicaciones.com <BR>
    <STRONG>Página web:<br></STRONG> www.intalnettelecomunicaciones.com<br>
</div>  

</P>
<H3 style="text-align:left">Horarios de atención:</H3>
<STRONG>Lunes a Viernes:</STRONG> 8:00 am a 12:00 pm y 2:00 pm a 6:00 pm<br>
<STRONG>Sábados:</STRONG> 8:00 am a 12:00 pm<br>



<p>Régimen de protección de los derechos de los usuarios de servicios de telecomunicaciones.</p>
<div class="col-md-6">
<i class="fa fa-file-pdf-o"></i> Seguridad en la Red &nbsp;&nbsp;&nbsp;<a href="pdf/seguridad_en_la_red.pdf" target="_blank"><i class="glyphicon glyphicon-download-alt"></i>Descargar</a><br>
<i class="fa fa-file-pdf-o"></i> Bloquear sitios en Internet Explorer y Firefox &nbsp;&nbsp;&nbsp;<a href="pdf/bloquear_sitios_en_internet_explorer_y_firefox.pdf" target="_blank"><i class="glyphicon glyphicon-download-alt"></i>Descargar</a><br>
<i class="fa fa-file-pdf-o"></i> Riesgos en la Red Malware&nbsp;&nbsp;&nbsp;<a href="pdf/riesgo_en_la_red_malware.pdf" target="_blank"><i class="glyphicon glyphicon-download-alt"></i>Descargar</a>
</div>

<div class="col-md-6">
<i class="fa fa-file-pdf-o"></i> Proteja su red - Filtro y Antispam&nbsp;&nbsp;&nbsp;<a href="pdf/proteja_su_red-filtro_antivirus_y_antispam.pdf" target="_blank"><i class="glyphicon glyphicon-download-alt"></i>Descargar</a><br>
<i class="fa fa-file-pdf-o"></i> Uso de redes inalámbricas en forma segura&nbsp;&nbsp;&nbsp;<a href="pdf/uso_de_redes_inalambricas_en_forma_segura.pdf" target="_blank"><i class="glyphicon glyphicon-download-alt"></i>Descargar</a><br>

</div>

</div>
@endsection