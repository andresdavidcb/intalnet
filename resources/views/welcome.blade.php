@extends('layouts.app')

@section('content')
<div class="container col-md-10 col-md-offset-1">
<div id="myCarousel" class="carousel slide" data-ride="carousel">

  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
  </ol>

  <div class="carousel-inner">
    <div class="item active">
      <img src="img/gallery/i5.png" alt="Los Angeles">
    </div>

    <div class="item">
      <img src="img/gallery/i6.png" alt="Chicago">
    </div>

    
  </div>

  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>


<h1>NUESTROS PLANES</h3>
 <div class="card col-md-4">
  <h3><i class="fa fa-wifi"></i></h3>
  <h3>INTERNET DOMICILIARIO Y/O COMERCIAL 2 MEGAS</h3>
  <p class="info">Chatea, navega y comparte en redes sociales con amigos y familiares sin límites 
    durante todo el mes.</p>
    <p class"details">Velocidad de acceso de bajada:<br>
      2.048Mbps<br>
      <p>Velocidad de subida:<br>
        1.024 Mbps</p>
  </div>


  <div class="card col-md-4">
  <h3><i class="fa fa-wifi"></i></h3>
  <h3>INTERNET DOMICILIARIO Y/O COMERCIAL 3 MEGAS</h3>
  <p class="info">Chatea, navega y comparte en redes sociales con amigos y familiares sin límites 
    durante todo el mes.</p>
    <p class="details">Velocidad de acceso de bajada:<br>
      3.072 Mbps<br>
      <p>Velocidad de subida:<br>
        1.536 Mbps</p>
  </div>

<div class="card col-md-4">
  <h3><i class="fa fa-wifi"></i></h3>
  <h3>INTERNET DOMICILIARIO Y/O COMERCIAL 4 MEGAS</h3>
  <p class="info">Chatea, navega y comparte en redes sociales con amigos y familiares. 
    Disfruta de tus películas online, y realiza video conferencias con tus amigos.</p>
    <p class="details">Velocidad de acceso de bajada:<br>
      4.096 Mbps<br>
      <p>Velocidad de subida:<br>
        2.048 Mbps</p>
  </div>



 <div class="card col-md-4">
  <h3><i class="fa fa-wifi"></i></h3>
  <h3>INTERNET DOMICILIARIO Y/O COMERCIAL 5 MEGAS</h3>
  <p class="info2">Navega y Chatea con tus amigos y familiares sin limites durante todo el mes. 
    Disfruta de películas online, realiza video conferencias con tus amigos. 
    Perfecto para cámaras de seguridad que requieren monitoreo constante.
  </p>
    <p class"details">Velocidad de acceso de bajada:<br>
      5.120 Mbps<br>
      <p>Velocidad de subida:<br>
        2.560 Mbps</p>
  </div>
  <div class="card col-md-4">
  <h3><i class="fa fa-wifi"></i></h3>
  <h3>INTERNET DOMICILIARIO Y/O COMERCIAL 8 MEGAS</h3>
  <p class="info2">Aprovecha la velocidad de Internet, para conectar tus amigos a tu red. 
    Esta velocidad permite trabajar de forma simultanea en varios dispositivos con rapidez y 
    seguridad. Útil para negocios.
</p>
    <p class="details">Velocidad de acceso de bajada:<br>
      8.240 Mbps<br>
      <p>Velocidad de subida:<br>
        4.120 Mbps</p>
  </div>

<div class="card col-md-4">
  <h3><i class="fa fa-wifi"></i></h3>
  <h3>INTERNET DOMICILIARIO Y/O COMERCIAL DEDICADO</h3>
  <p class="info2">Es un servicio simétrico de conectividad 100% con un ancho de banda garantizado
   y de forma segura. Es un producto ideal para Empresas, instituciones o negocios, que permite la 
   navegación permanente.
</p>
    <p class="details">&nbsp;<br>
      &nbsp;<br>
      <p>&nbsp;<br>
        &nbsp;</p>
  </div>



<div class="card2 col-md-12">
<div class="col-md-3">
  <h3><a href="http://speedtest.googlefiber.net/" target="_blank"><img src="img/gallery/speed-test2.png" width="100%" style="border-radius:15px;border:1px solid #fff"></a></h3>
</div>

<div class="col-md-9">
<h3>Mide la velocidad de tu internet</h3>
  <p>Muchas veces las personas se preguntan ¿Porque esta lento mi Internet? y existen multiples factores 
    que pueden hacer que su internet este lento, uno de ellos puede ser el estar viendo videos en 
    Youtube o descargando música entre otros.</p>

<p>Pero lo más importante es que sepas de cuanto es la velocidad de Internet que te esta ofeciendo 
  tu proveedor o ISP.
  </p>
  <a href="http://speedtest.googlefiber.net/" target="_blank"><I class="glyphicon glyphicon-signal"></I> Mide la velocidad de Internet aquí</a>

</div>



  
  </div>

  <h1>INTALNET TE DA LO MEJOR</h1>
<div class="col-md-4"><img src="img/gallery/b1.png" style="padding-bottom:25px"></div>
<div class="col-md-4"><img src="img/gallery/b2.png"></div>
<div class="col-md-4"><img src="img/gallery/b3.png"></div>

<h1>CUIDADOS EN LA RED</h1>
<P>En caso de detectar en la red algún tipo de amenaza que atente contra su integridad y la de los 
  suyos, comuníquese por favor con las siguientes instituciones oficiales encargadas para tal fin, 
  donde también podrá acceder a las Líneas de Atención en Prevención de la Pornografía Infantil.
</P>
<div class="col-md-4">
<div class="col-md-6"><a href="http://www.fiscalia.gov.co/colombia/" target="_blank"><img src="img/style/fiscalia.png"></a></div>
<div class="col-md-6"><a href="https://www.policia.gov.co/dijin" target="_blank"><img src="img/style/dijin.png"></a></div>
<div class="col-md-6"><a href="http://www.icbf.gov.co/portal/page/portal/PortalICBF/" target="_blank"><img src="img/style/bienestar.png"></a></div>
<div class="col-md-6"><a href="http://www.mintic.gov.co/portal/604/w3-article-720.html" target="_blank"><img src="img/style/internet-sano.png"></a></div>


</div>
<div class="card2 col-md-8">
<h2>TODOS POR UN MEJOR PAÍS</h2>
<div class="col-md-3"><a href="https://www.crcom.gov.co/es/pagina/inicio" target="_blank"><img src="img/style/crc.png" width="80%"></a></div>
<div class="col-md-3"><a href="http://www.mintic.gov.co/portal/604/w3-channel.html" target="_blank"><img src="img/style/mintic_3.png" width="100%"></a></div>
<div class="col-md-3"><a href="http://www.gobiernoenlinea.com.co/portal/604/w3-channel.html" target="_blank"><img src="img/style/gov.png" width="100%"></a></div>
<div class="col-md-3"><a href="https://www.dnp.gov.co/Paginas/%E2%80%9CTodos-por-un-nuevo-pa%C3%ADs%E2%80%9D,-la-ruta-para-cumplir-las-metas-del-Plan-Nacional-de-Desarrollo-2014-2018.aspx" target="_blank"><img src="img/style/dnp.png" width="100%"></a></div>
<div class="col-md-3"><a href="http://es.presidencia.gov.co/Paginas/presidencia.aspx" target="_blank"><img src="img/style/presidencia.png" width="100%"></a></div>
<div class="col-md-3"><a href="http://www.mintic.gov.co/portal/vivedigital/612/w3-channel.html" target="_blank"><img src="img/style/vive_digital.png" width="100%"></a></div>
<div class="col-md-3"><a href="https://www.nomasfilas.gov.co/" target="_blank"><img src="img/style/si_virtual.png" width="100%"></a></div>
<div class="col-md-3"><a href="https://www.procuraduria.gov.co/portal/" target="_blank"><img src="img/style/procuraduria.png" width="100%"></a></div>
</div>

</div>

@endsection