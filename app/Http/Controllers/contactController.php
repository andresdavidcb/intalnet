<?php

namespace valencianet\Http\Controllers;

use Illuminate\Http\Request;
use valencianet\Http\Requests;

use Mail;
use Session;
use Redirect;

class contactController extends Controller
{
    public function index()
    {
        return view('contacto.index');
    }

    public function store(Request $request)
    {
        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['msg'] = $request->msg;
        Mail::send('mails.users', ['data' => $data], function ($m) use ($data) {
            
            $m->to($data['email'], $data['name'])
            ->subject('notificación');
        });
        Mail::send('mails.admin', ['data' => $data], function ($m) use ($data) {
            $m->from($data['email'], $data['name'])
            ->to('servicioalcliente@intalnettelecomunicaciones.com', 'INTALNET TELECOMUNICACIONES')
            ->subject('contacto');
        });
        return Redirect::to('/contacto');
    }
}
