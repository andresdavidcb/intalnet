@extends('layouts.app')
@section('content')
<div class="container col-md-10 col-md-offset-1">
<H1>ENTES DE CONTROL</H1>
<H2>Ministerio de Tecnologías de la Información y las Comunicaciones MINTIC</H2>
<div class="col-md-2">
<img src="img/gallery/entes.png" width="100%">
</div>
<div class="col-md-10">
<P>
 <strong>Dirección:</strong><br>Edificio Murillo Toro, Carrera 8 Calle 1. 
 Bogotá, Colombia<br>
 <strong>Teléfono:</strong><br> PBX +(571)344 34 60<br>
 <strong>Página Web:</strong>
 <a href="http://www.mintic.gov.co">www.mintic.gov.co</a><br>
 <strong>Código Postal:</strong> 111711
</P>
</div>
</div>
@endsection