<div class="form-group{{ $errors->has('module') ? ' has-error' : '' }}">
    <label for='module' class='col-md-2 control-label'>Menú</label>
    <div class='col-md-10'>
   <select name='module' class='form-control'>
            @foreach($module as $field)
            @if((isset($form)&&$form->module==$field->id)||$field->id==old('module'))
            <option value="{{$field->id}}" selected>
            @else
            <option value="{{$field->id}}">
            @endif   
                {{$field->name}}</option>
            @endforeach
       </select>
          @if ($errors->has('module'))
        <span class='help-block'><strong>{{ $errors->first('module') }}</strong></span>
    @endif
    </div>
</div>

<div class="form-group{{ $errors->has('route') ? ' has-error' : '' }}">
    <label for='route' class='col-md-2 control-label'>Ruta</label>
    <div class='col-md-10'>
    <input type='text' id='route' name='route' class='form-control' value="{{isset($form)?$form->route:old('route')}}">
     @if ($errors->has('route'))
        <span class='help-block'><strong>{{ $errors->first('route') }}</strong></span>
    @endif
    </div>
</div>

<div class="form-group{{ $errors->has('model') ? ' has-error' : '' }}">
    <label for='model' class='col-md-2 control-label'>Modelo</label>
    <div class='col-md-10'>
    <input type='text' id='model' name='model' class='form-control' value="{{isset($form)?$form->model:old('model')}}">
     @if ($errors->has('model'))
        <span class='help-block'><strong>{{ $errors->first('model') }}</strong></span>
    @endif
    </div>
</div>

<div class="form-group{{ $errors->has('table_label') ? ' has-error' : '' }}">
    <label for='table_label' class='col-md-2 control-label'>Etiqueta</label>
    <div class='col-md-10'>
    <input type='text' id='table_label' name='table_label' class='form-control' value="{{isset($form)?$form->table_label:old('table_label')}}">
     @if ($errors->has('table_label'))
        <span class='help-block'><strong>{{ $errors->first('table_label') }}</strong></span>
    @endif
    </div>
</div>

<div class="form-group{{ $errors->has('fields') ? ' has-error' : '' }}">
    <label for='fields' class='col-md-2 control-label'>Campos a mostrar</label>
    <div class='col-md-10'>
    <textarea id='fields' name='fields' class='form-control'>{{isset($form)?$form->fields:old('fields')}}</textarea>	
     @if ($errors->has('fields'))
        <span class='help-block'><strong>{{ $errors->first('fields') }}</strong></span>
    @endif
    </div>
</div>

<div class="form-group{{ $errors->has('create_fields') ? ' has-error' : '' }}">
    <label for='create_fields' class='col-md-2 control-label'>Campos Form. base</label>
    <div class='col-md-10'>
    <textarea id='create_fields' name='create_fields' class='form-control'>{{isset($form)?$form->create_fields:old('create_fields')}}</textarea>
     @if ($errors->has('create_fields'))
        <span class='help-block'><strong>{{ $errors->first('create_fields') }}</strong></span>
    @endif
    </div>
</div>