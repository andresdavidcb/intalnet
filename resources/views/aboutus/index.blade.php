@extends('layouts.app')

@section('content')
<div class="container col-md-10 col-md-offset-1">

<h1>¿QUIÉNES SOMOS?</h1>


<div class="col-md-5">
  
  <p>Somos INTALNET TELECOMUNICACIONES, creada a partir del 11 de septiembre de 2015 en Montería - Córdoba, 
    una empresa prestadora de servicios de telecomunicaciones para cubrir las necesidades de conexión y 
    tecnologías de la información en algunos municipios del departamento de Córdoba.
  </p>

  <img src="img/gallery/usando_internet.png" width="100%">

  

</div>
<div class="col-md-7">
  <div id="myCarousel" class="carousel slide" data-ride="carousel">

  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    
  </ol>

  <div class="carousel-inner">
    <div class="item active">
      <img src="img/gallery/i3.jpg" alt="Los Angeles">
    </div>

    <div class="item">
      <img src="img/gallery/i4.jpg" alt="Chicago">
    </div>

    

  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>
</div>


</div>
@endsection