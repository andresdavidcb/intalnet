@extends('layouts.app')

@section('content')
<div class="container col-md-10 col-md-offset-1">
<H1>POLÍTICAS DE SEGURIDAD</H1>



<div class="container col-md-4">
  
<P>
  <i class="fa fa-file-pdf-o"></i> Internet Sano &nbsp;&nbsp;&nbsp;<a href="pdf/internet_sano.pdf" target="_blank"><i class="glyphicon glyphicon-download-alt"></i>Descargar</a><br>
<i class="fa fa-file-pdf-o"></i> Control Parenteral &nbsp;&nbsp;&nbsp;<a href="pdf/control_parenteral.pdf" target="_blank"><i class="glyphicon glyphicon-download-alt"></i>Descargar</a><br>
  

</P>

</div>


<div class="col-md-8">
<img src="img/gallery/politicas_seguridad.png">
</div>

</div>
@endsection