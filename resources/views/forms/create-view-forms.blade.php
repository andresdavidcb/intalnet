@extends('layouts.app')
@section('content')
<style type="text/css">
textarea{height: 150px;width: 100%}
</style>

<div class='container col-md-12'>

<?php
$module=$_POST['module'];
$modules=[null,"admin","accounting","events","thirds"];
$path='http://www.oasisacademia.com/resources/views/'.($_POST["route"]."/");

#FORMULARIO PARA INDEX.BLADE.PHP
###########################################################
$a="@extends('layouts.app')
@section('content')
@include('layouts.".$modules[$module]."')
<div class='success'></div>
<div class='container col-md-10'>

    <div class='col-md-3'>
    <a href=\"{{ route('".$_POST['route'].".create') }}\" ><span class='glyphicon glyphicon-plus'></span>Creación de ".$_POST['table_label']."</a>
    </div>
    <a href=\"{{ url('".$_POST['route']."/pdf') }}\"><i class='fa fa-file-pdf-o' aria-hidden='true'></i> Lista de usuarios</a>
    
    <table class='table' id='myTable'>
        <thead><tr>
        ";
            $fields=explode(',', $_POST['fields']);
            $thead="";
            foreach ($fields as $key => $value) {
            $arrayField=explode(':', $value);
            $label=$arrayField[0];
            $a = $a."<th>".$label."</th>";
            }
            
$a= $a.
        '<th>Acciones</th>  
        </tr></thead>

        <tbody>
            @foreach($'.$_POST['route'].' as $'.$_POST['model'].')<tr>'."\n";
            foreach ($fields as $key => $value) {
                $arrayField=explode(':', $value);
                if(isset($arrayField[1]))$compactvar=$arrayField[1];else $compactvar=$arrayField[0];
                $fieldsType=explode('->', $compactvar);
                $gvar=$fieldsType[0];
                $a = $a."           <td>{{ $".$_POST['model']."->$gvar }}</td>\n";
              }
            $a = $a."           <td>
            <a href=\"{{ route('".$_POST['route'].".edit',$".$_POST['model']."->id) }}\" title='Modificar'><span class='glyphicon glyphicon-pencil btn btn-danger' style='padding:1px 3px'></span></a>
            <a href=\"{{ url('".$_POST['route']."/anulate',$".$_POST['model']."->id) }}\" title='Anular'><span class='glyphicon glyphicon-remove btn btn-danger' style='padding:1px 3px'></span></a>
            </td>
            
            </tr>
            @endforeach
        </tbody>        
    </table>
    <p><strong>Total registros activos: </strong>{{".'$total'."}}</p>
</div>  
@endsection";

if(!file_exists($path)) mkdir($path,0777,true);
$file=fopen($path."index.blade.php", 'w') or die ("error creando archivo");
if(fwrite($file, $a))echo "Formulario 'index' generado<br>";


#FORMULARIO PARA BASEFORM.BLADE.PHP
##########################################
$fields=explode(',', $_POST['create_fields']);
$a="";
    foreach ($fields as $key => $value) {
    $arrayField=explode(':', $value);
    $label=$arrayField[0];
    $gvars=explode("-", $arrayField[1]);
    $var=$gvars[0];
    $datatype = (isset($gvars[1])) ? $gvars[1] : 'text' ;
    $a=$a.
"<div class=\"form-group{{ \$errors->has('$var') ? ' has-error' : '' }}\">
    <label for='$var' class='col-md-3 control-label'>$label</label>
    <div class='col-md-9'>
";
    switch ($datatype) {
        case 'textarea':
            $a= $a."    <textarea id='$var' name='$var' class='form-control'>{{isset(\$".$_POST['model'].")?\$".$_POST['model']."->$var:old('$var')}}\"</textarea>\n";
            break;
        case 'select':
            $a= $a."   <select name='$var' class='form-control'>
            @foreach(\$$var as \$field)
            @if((isset(\$".$_POST['model'].")&&\$".$_POST['model']."->$var==\$field->id)||\$field->id==old('$var'))
            <option value=\"{{\$field->id}}\" selected>
            @else
            <option value=\"{{\$field->id}}\">
            @endif   
                {{\$field->name}}</option>
            @endforeach
       </select>
       ";break;
        default:
            $a= $a."    <input type='$datatype' id='$var' name='$var' class='form-control' value=\"{{isset(\$".$_POST['model'].")?\$".$_POST['model']."->$var:old('$var')}}\">\n";
            break;
    }
    $a= $a."     @if (\$errors->has('$var'))
        <span class='help-block'><strong>{{ \$errors->first('$var') }}</strong></span>
    @endif
    </div>
</div>

";
    }

if(!file_exists($path)) mkdir($path,0777,true);
$file=fopen($path."baseForm.blade.php", 'w') or die ("error creando archivo");
if(fwrite($file, $a))echo "Formulario 'baseform' generado!<br>";


#FORMULARIO PARA CREATE.BLADE.PHP
###########################################
$a="@extends('layouts.app')
@section('content')
@include('layouts.".$modules[$module]."')
<div class='container col-md-10'>
    <div class='row'>
            <div class='panel panel-default'>
                <div class='panel-heading'>Creación de ".$_POST['table_label']."</div>
                <div class='panel-body'>
                    {!! Form::open(['route' => '".$_POST['route'].".store']) !!}
                        @include('".$_POST['route'].".baseForm')
                        <div class='form-group'>
                            <div class='col-md-10'>
                                <button type='submit' id='btn_".$_POST['model']."' class='btn btn-danger'><i class='glyphicon glyphicon-save'></i> Registrar</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
    </div>
</div>
@endsection";


if(!file_exists($path)) mkdir($path,0777,true);
$file=fopen($path."create.blade.php", 'w') or die ("error creando archivo");
if(fwrite($file, $a))echo "Formulario 'create' generado!<br>";


#FORMULARIO PARA EDITAR
######################################
$a="@extends('layouts.app')
@section('content')
@include('layouts.".$modules[$module]."')
<div class='container col-md-10'>
    <div class='row'>
            <div class='panel panel-default'>
                <div class='panel-heading'><b>Edición de ".$_POST['table_label']."</b> <span class='glyphicon glyphicon-arrow-right'></span> {{\$".$_POST['model']."->name}}</div>
                <div class='panel-body'>
                    {!! Form::model(\$".$_POST['model'].",['route' => ['".$_POST['route'].".update',$".$_POST['model']."->id],'method'=>'PUT']) !!}
                    @include('".$_POST['route'].".baseForm')
                        <div class='form-group'>
                            <div class='col-md-10'>
                                <button type='submit' class='btn btn-danger'>
                                    <i class='glyphicon glyphicon-save'></i> Guardar
                                </button>
                            </div>
                        </div>
                        <div class='form-group'>
                            <div class='col-md-1'>
                                <a href=\"{{route('".$_POST['route'].".index')}}\" class='btn btn-danger' style='margin-left:10px'>
                                    <i class='glyphicon glyphicon-remove-sign'></i>Cancelar
                                </a>
                            </div>
                        </div>
                    {!!Form::close()!!}
                </div>
            </div> 
    </div>
</div>
@endsection";

if(!file_exists($path)) mkdir($path,0777,true);
$file=fopen($path."edit.blade.php", 'w') or die ("error creando archivo");
if(fwrite($file, $a))echo "Formulario 'edit' generado!<br>";
?>

<a href="{{ url('forms') }}" class="btn btn-danger">Continuar</a>
</div>
@endsection