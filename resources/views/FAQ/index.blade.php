@extends('layouts.app')
@section('content')
<style>
h2,h3{text-align:left;font-weight:bold}
</style>
<div class="container col-md-10 col-md-offset-1">

	<H1>PREGUNTAS FRECUENTES</H1>
	<h2>Preguntas enfocadas a la atención de los usuarios durante la novedad del COVID-19</h2>
	<ol>
		<li><H3>¿Los servicios de internet y televisión son servicios esenciales?</H3></li>
		<P>SÍ.</P>
		<P>El MinTIC expide el Decreto 464 del 23 de marzo de 2020 declarando que los servicios de internet y televisión son esenciales (artículo 1º)</P>

		 <li><H3>¿Los operadores de internet y televisión pueden suspender labores de instalación, mantenimiento y adecuación de las redes requeridas para la operación del servicio?</H3></li>
		<P>NO.</P>
		<P>En el artículo 1º del Decreto 464 del 23 de marzo de 2020 expedido por el MinTIC se establece que los PRST “…no podrán suspender las labores de instalación, mantenimiento y adecuación de las redes requeridas para la operación del servicio.”</P>

		<li><H3>¿Siendo los servicios de internet y televisión esenciales deben ser prestados por los operadores gratuitamente? </H3></li>
		<P>NO.</P>
		<P>La normatividad de emergencia no suspende lo dispuesto en el Artículo 2.1.12.1 del Capítulo 1 del Título II de la Resolución CRC 5050 de 2016. Así las cosas, los Usuarios están obligados a pagar oportunamente la factura por la prestación de los servicios de internet y televisión por suscripción. </P>

		<li><H3>¿Los operadores pueden cortar la prestación de los servicios de internet y televisión por falta de pago?</H3></li>
		<P>SÍ.</P>
		<P>La CRC mediante la Resolución No. 5951 del 26 de marzo de 2020 dispone que los usuarios de televisión por suscripción e internet tienen la obligación de pagar oportunamente la factura. </P>
		<P>Ahora bien, bajo la premisa que son servicios esenciales esta Comisión exhorta a los operadores a que les brinden a los Usuarios facilidades de pago. </P>

		<li><H3>¿Los operadores de internet y televisión pueden cortar la prestación de los servicios cuando pese a las facilidades de pago los Usuarios se niegan a pagar?</H3></li>
		<P>La Resolución CRC No. 5951 del 26 de marzo de 2020 no releva a los Usuarios de la obligación de pago oportuno.</P>
		<P>INTALNET TELECOMUNICACIONES (Inversiones Zuluaga Sejin S.A.S) avisará a los Usuarios los cortes del servicio que realizará por falta de pago.</P>

		<li><H3>¿Los operadores de internet y televisión están obligados a condonar las deudas de los usuarios?</H3></li>
		<P>NO.</P>
		<P>En el artículo 4 de la Resolución CRC No. 5951 del 26 de marzo de 2020 se establece que durante el término que perdure la emergencia no podemos cobrar intereses por mora. Lo anterior no implica condonaciones de las sumas que los usuarios deben.</P>

		<li><H3>¿Cuáles son los canales con los que cuenta el Usuario para presentar una PQR´s?</H3>
		<P>Con el propósito de que el usuario se quede en casa, hasta el 31 de mayo de 2020 se habilitaron los siguientes canales para presentación de las PQR´s.</P>
		  <UL>
			<li>En nuestra página web <a href="http://www.intalnettelecomunicaciones.com/PQR">http://www.intalnettelecomunicaciones.com/PQR</a></li>
			<li>Teléfono celular y whatsapp: 315 2522215 (Sede Valencia) y 311 3934218 (Sede Tierralta).</li>
			<li>Redes sociales en Facebook:  @Valencia Net y @Intal Net</li>
			<li>Redes sociales en Instagram:  @intalnet_valencia y @intanet_tierralta</li>
		  </UL>
		</li>  
		<li><H3>¿Cuál es el horario en que el Usuario puede presentar su PQRs? </H3></li>
		<P>La CRC mediante Resolución 5951 del 26 de marzo de 2020 flexibilizó el horario de atención a través de líneas telefónicas de 8:00am a 6:00pm los 7 días de la semana.</P>
		<P>Para los casos de reporte de fallas la atención telefónica se realizará los 7 días de la semana 24 horas al día.</P>
		<P>Con lo anterior, no se modifican los tiempos de respuesta (<a href="http://www.intalnettelecomunicaciones.com/PQR">http://www.intalnettelecomunicaciones.com/PQR</a>) ni la garantía de atender satisfactoriamente a nuestros Usuarios.</P>

		<li><H3>¿La facturación y las respuestas a las PQRs pueden ser enviadas al correo electrónico del Usuario? </H3></li>
		<P>SÍ.</P>
		<P>La Resolución CRC No. 5956 del 19 de marzo de 2020 permite que el operador emita la respuesta a la PQRs por correo electrónico, aunque no haya sido el mismo medio por el que se presentó o el Usuario no lo haya autorizado expresamente.</P>
		<P>Esta misma norma faculta al operador a enviar la factura por correo electrónico aun cuando el Usuario no lo haya autorizado.</P>
		<P>Si no se cuenta con el correo electrónico INTALNET TELECOMUNICACIONES (Inversiones Zuluaga Sejin S.A.S) entregará la factura y la respuesta de la PQRs en físico. </P>
		

		<li><H3>¿El Usuario puede presentar las solicitudes de cesión de contrato y/o soporte de equipos terminales por medios no físicos?</H3>
		<P>SÍ.</P>
		<P>La CRC mediante Resolución 5951 del 26 de marzo de 2020 permite que hasta el 31 de mayo de 2020 las solicitudes de cesión de contrato y/o soporte de equipos terminales se realicen por medios no físicos y en INTALNET TELECOMUNICACIONES (Inversiones Zuluaga Sejin S.A.S) se pueden hacer mediante:</P>
		  <ul>
		  <li>En nuestra página web <a href="http://www.intalnettelecomunicaciones.com/PQR">http://www.intalnettelecomunicaciones.com/PQR</a></li>
			<li>Teléfono celular y whatsapp: 315 2522215 (Sede Valencia) y 311 3934218 (Sede Tierralta).</li>
		  </ul>
		</li>

		<li><H3>¿En caso de que el Usuario tenga inconvenientes con la prestación de sus servicios (televisión y/o internet) primero debe acudir a la Superintendencia de Industria y Comercio – SIC?</H3></li>
		<P>NO.</P>
		<P>La Resolución SIC No. 19012 de 2020 enfatiza que INTALNET TELECOMUNICACIONES (Inversiones Zuluaga Sejin S.A.S)  es el primer canal de solución. Ahora bien, solo en el caso que no demos respuesta oportunamente el Usuario podrá acudir a la SIC.</P>
		<P>Para más información puedes consultar este link: <br>
		<a href="https://www.sic.gov.co/atención-al-ciudadano/canales-de-atención">https://www.sic.gov.co/atención-al-ciudadano/canales-de-atención</a>
	
	<li><H3>¿Qué es ancho de banda?</H3></li>
	<div class="col-md-10">
		<P>Dicho de forma muy simple, el ancho de banda es el caudal o capacidad de transmisión de datos que soporta un enlace. Suele ser un recurso compartido por numerosos usuarios (por ejemplo, todos los usuarios de un determinado proveedor).
		</P>	
	</div>
	<div class="col-md-2">
		<img src="img/gallery/banda_ancha.png" width="100%">
	</div>


	<li><H3>¿Qué es conexión WIFI?</H3></li>
	<div class="col-md-10">
		<P>Wifi o Wi-Fi es originalmente una abreviación de la marca comercial Wireless Fidelity, que en inglés significa 'fidelidad sin cables o inalámbrica'. La comunicación inalámbrica, como tal, es aquella que prescinde de cables o medios físicos visibles de propagación, y que, por el contrario, emplea ondas electromagnéticas para su trasmisión, siendo que esta, no obstante, estará limitada a un radio específico de cobertura.
		</P>	
		
	</div>
	<div class="col-md-2">
		<img src="img/gallery/conexion_wifi.png" width="100%">
	</div>


	<li><H3>¿Qué es una mega?</H3></li>
	<div class="col-md-10">
		<P>Con esta palabra puede hacerse referencia tanto a la velocidad de una conexión como a la cantidad de información en bytes. Una conexión a Internet con bajada de 2 megas transfiere 2 millones de bits por segundo, por lo tanto, 2 millones dividido 8, resulta en 200.000 bytes por segundo, o sea 200 kb/seg.
		</P>	
		
	</div>
	<div class="col-md-2">
		<img src="img/gallery/mega.png" width="100%">
	</div>


	<li><H3>¿Qué es un router?</H3></li>
	<div class="col-md-10">
		<P>Un router es un dispositivo de red que se encarga de llevar por la ruta adecuada el tráfico. En tu casa seguramente tendrás uno que es el que te conecta con Internet. Los routers funcionan utilizando direcciones IP para saber a donde tienen que ir los paquetes de datos cuando se compra el servicio.
		</P>	
		
	</div>
	<div class="col-md-2">
		<img src="img/gallery/router2.png" width="70%" style="margin-left:15%">
	</div>



</div>

@endsection