<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('somos', 'aboutusController@index');
Route::get('mision', 'misionController@index');
Route::get('vision', 'visionController@index');
Route::get('valores', 'valuesController@index');
Route::get('licencia', 'licController@index');
Route::get('FAQ', 'faqController@index');
Route::get('proteccion_al_usuario', 'proteccionController@index');
Route::get('entes_de_control', 'entesController@index');
Route::get('politicas', 'politicasController@index');
Route::get('seguridad_en_la_red', 'intSanoController@index');
Route::resource('PQR', 'pqrController');
Route::resource('contacto', 'contactController');
