@extends('layouts.app')
@section('content')

<div class="container col-md-10 col-md-offset-1">

	<H1>SEGURIDAD EN LA RED</H1>
	<div class="col-md-7">
		<h3>
			<img src="img/gallery/internet_sano.png">
		</h3>
		<p>Los niños cada vez tienen acceso a las nuevas tecnologías en edades más tempranas ya que principalmente las usan para llegar hasta recursos educativos o de entretenimiento. Sin embargo, dejar al niño sólo con una tablet o un Smartphone o computador sin llevar a cabo unas simples medidas de seguridad, les puede exponer a ciertos peligros.
		</p>
		<p>Es importante que los padres eduquemos a nuestros hijos y les avisemos de los riesgos que supone la navegación en Internet y el uso de las redes sociales. Saber qué puede ocurrir les mantendrá alerta.
		</p>
		
	</div>
	<div class="col-md-5">
		<img src="img/gallery/internet_sano2.png" width="100%" style="margin-bottom:20px">

	</div>


	<h1>PROTECCIÓN CONTRA LA PORNOGRAFÍA INFANTIL</h1>
	<P>INTALNET TELECOMUNICACIONES (Inversiones Zuluaga Sejin S.A.S) como parte de su compromiso social y en
	 cumplimiento de las normas vigentes, informa a sus usuarios sobre la existencia y los alcances
	  de la Ley 679 de 2001, y sus decretos reglamentario. Si desea conocer el texto completo de la Ley, 
	  puede ingresar a la secretaría del senado en: 
	  <A HREF="http://www.secretariasenado.gov.co">http://www.secretariasenado.gov.co</A>
	</P>

	<P>Internet Sano es la campaña del Ministerio de Comunicaciones por medio de la cual se pretende 
		generar opinión y conocimiento en torno al tema de la explotación infantil en Internet, 
		destacando mensajes de prevención, de denuncia, informativos e institucionales. Esta campaña 
		del Ministerio de Comunicaciones busca que todos los Colombianos comprendan y asimilen lo que 
		significa la prevención de la pornografía infantil y juvenil en Internet. Dentro de los 
		desarrollos de esta campaña y en cumplimiento de la Ley 679 de 2001 y del Decreto 1524 de 2004,
		 Colombia Telecomunicaciones adoptó el Código de Conducta para el manejo y aprovechamiento de 
		 redes globales de información y la protección de los menores de edad contra la pornografía 
		 infantil con el objetivo de prevenir la divulgación de material de pornografía infantil a 
		 través de las redes y equipos de cómputo bajo su directa administración, con el fin de 
		 prevenir el acceso de menores de edad a cualquier tipo de material pornográfico, 
		 como herramienta para la lucha contra la pornografía infantil.</P>
<P>En los siguientes lugares de Internet se pueden realizar las denuncias relacionadas con sitios y 
	contenidos de pornografía de menores de edad y páginas electrónicas en las que se ofrezcan 
	servicios sexuales con niños:</P>


<div class=" card2 col-md-12">	
<H3>Ministerio TIC</H3>
<P>En TIC confío es la campaña del Ministerio de Comunicaciones para que todos los colombianos 
	comprendamos el significado de la prevención de la pornografía infantil y juvenil en Internet.</P>
	
	<div class="col-md-4">
	<STRONG>Páginas web:</strong><br>
		<a href="http://www.enticconfio.gov.co">www.enticconfio.gov.co</a><br>
		<a href="http://www.teprotejo.org">www.teprotejo.org</a><br>
		<a href="http://www.internetsano.gov.co">www.internetsano.gov.co</a><br> 	   
	</div>

	<div class="col-md-4">
	<STRONG>Teléfonos:</strong><br>
		3443460 en Bogotá<br>
		01 8000 914 014 para el resto del país<br>
		<br> 	   
	</div>
</div>


<div class=" card2 col-md-12">	
<H3>INSTITUTO COLOMBIANO DE BIENESTAR FAMILIAR: ICBF</H3>

	
	<div class="col-md-4">
	<STRONG>Páginas web:</strong><br>
		<a href="http://www.icbf.gov.co">www.icbf.gov.co</a><br>
			   
	</div>

	<div class="col-md-8">
	<STRONG>Linea gratuita nacional: Prevención de abuso sexual</strong><br>
		(57 1) 01 8000 112 440<br>
	<STRONG>Desde celular marca:</strong><br>
		144<br>
			   
	</div>
</div>


<div class=" card2 col-md-12">	
<H3>FISCALÍA GENERAL DE LA NACIÓN</H3>

	
	<div class="col-md-4">
	<STRONG>Páginas web:</strong><br>
		<a href="http://www.fiscalia.gov.co">www.fiscalia.gov.co</a><br>
			   
	</div>

	<div class="col-md-8">
	<STRONG>Teléfono</strong><br>
		 01 8000 916 111<br>
	
			   
	</div>
</div>	



<div class=" card2 col-md-12">	
<H3>GRUPO INVESTIGATIVO DE DELITOS INFORMÁTICOS</H3>

	<div class="col-md-4">
		
	<STRONG>Páginas web:</strong><br>
		<a href="http://portal.policia.gov.co/">portal.policia.gov.co</a><br>
	<STRONG>correo electrónico:</strong><br>
		 lineadirecta@policia.gov.co<br>		   
	</div>
	<div class="col-md-8">
	<STRONG>Dirección:</strong><br>
		Carrera 59 N. 26‐21 CAN, Bogotá D.C.<br>
	<STRONG>Linea gratuita nacional:</strong><br>
		 01 8000 910 112<br>
	 
	</div>		
	

	
	
			   
	</div>
</div>
</div>

@endsection