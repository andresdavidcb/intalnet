@extends('layouts.app')
@section('content')

<div class="container col-md-10 col-md-offset-1">

	<H1>NUESTRA VISIÓN</H1>
	<div class="col-md-5">
		<p>Ser la empresa líder en la región del Alto Sinú, con tecnología a la vanguardia de las telecomunicaciones, reconocidos por la calidad del servicio, la excelencia y el acompañamiento permanente.
		</p>
		<h3><img src="img/gallery/usando_internet2.png"></h3>
	</div>
	<div class="col-md-7">
		<img src="img/gallery/i5.png" width="100%">

	</div>

</div>

@endsection